import React, {Component} from 'react';
import superagent from 'superagent';

export default class Calendar extends Component {

    constructor(){
        super();
        this.state = { events: {} };
        this.refresh = this.refresh.bind(this);
        this.bookEvent = this.bookEvent.bind(this);
        this.setStudent = this.setStudent.bind(this);
    }

    bookEvent(slot){

        if(!this.state.studentName || !this.state.studentEmail){
            alert('Student details is not available');
            return;
        }
        var that = this;
        console.log('slot is ', slot);
        superagent.post('/bookEvent')
        .query({email: this.props.params.userId})
        .query({studentName: this.state.studentName})
        .query({studentEmail: this.state.studentEmail})
        .send({slot: slot})
        .end((err, res) => {
            if(err){
                if(res && res.text) alert(res.text);
                else alert('Error while booking slot');
            }
            else{
                console.log(res.body);
                that.refresh();
            }
        });
    }

    refresh(){
        superagent.get('/getEvents')
            .query({email: this.props.params.userId})
            .end((err, res) => {
                if(err){
                    console.log('Error getting calendar details for '+this.props.params.userId);
                }
                else{
                    //console.log('events is ', res.body);
                    //console.log('events string is ', JSON.stringify(res.body));
                    this.setState({events: res.body});
                }
            });

    }

    componentDidMount(){
        this.refresh();
        this.setState({user: this.props.params.userId});
    }

    setStudent(e){
        if(e.target.name == 'name') this.setState({studentName: e.target.value});
        else this.setState({studentEmail: e.target.value});
    }

    render(){

        console.log('props of calendar is ', this.props);
        var events = this.state.events;
        var eventList = (
            <table>
                <tr><th>Date</th><th>Start</th><th>End</th><th>Duration</th><th>Status</th></tr>
                {
                    Object.keys(events).map((day) => {

                        var list = events[day];

                        return list.map((event) => {
                            //console.log('event is ', event);
                            var start = new Date(event.start), end = new Date(event.end);
                            var duration = (end.valueOf() - start.valueOf())/(60*1000);
                            var booked = 'Booked';
                            if(!event.booked) booked = <button onClick={this.bookEvent.bind(undefined, event)}>Book</button>;
                            return  <tr><td>{day}</td><td>{start.toString().slice(16, 21)}</td><td>{end.toString().slice(16, 21)}</td><td>{duration+' Minutes'}</td><td>{booked}</td></tr>
                        })
                    })
                }
            </table>
        );

        return (
            <div>
                <h2>Calendar of {this.state.user} <button style={{float: 'right', 'fontSize': 'large'}} onClick={this.refresh}>Refresh</button></h2>
                <p>
                    <span style={{fontSize: 'large', marginRight: 10}}>Name</span>
                    <input className="studentInput" name="name" type="text" onChange={this.setStudent}/>
                </p>
                <p>
                    <span style={{fontSize: 'large', marginRight: 10}}>Email</span>
                    <input className="studentInput" name="email" type="text" onChange={this.setStudent}/>
                </p>
                {eventList}
            </div>
        );
    }
};