import React from 'react';
import _ from 'underscore';
import DateTime from 'react-datetime';
import superagent from 'superagent';
var CLIENT_ID = '133551892961-p9p0a5gg752o8k3uj4g7k2ps8c8erkv5.apps.googleusercontent.com';
import {Link} from 'react-router';


var SCOPES = ["https://www.googleapis.com/auth/calendar", "https://www.googleapis.com/auth/plus.me", "https://www.googleapis.com/auth/userinfo.email"];


export default class Hello extends React.Component {

    constructor(){
        super();
        this.onSignOut = this.onSignOut.bind(this);
        this.test = this.test.bind(this);
        this.state = {tableDisplay: 'none', authDisplay: 'block', done: true, email: null};
        //this.state = this.state.bind(this);
        this.addSlot = this.addSlot.bind(this);
        //this.setState = this.setState.bind(this);
        this.slotStart = this.slotStart.bind(this);
        this.slotEnd = this.slotEnd.bind(this);
        this.handleAuthClick = this.handleAuthClick.bind(this);
        this.getEvents = this.getEvents.bind(this);
    }

    componentWillMount() {
        this.callbackName = _.uniqueId("gPlusCallback-");
        //window['hacker'] = this.onSignIn;
        window['hacker'] = this.test;

    }

    componentWillUnmount() {
        delete window['hacker'];
    }

    shouldComponentUpdate(nextProps, nextState){
        if(typeof nextState.user == undefined) return false;
        return true;
    }

    test(e){
        console.log('test called', e);
        this.setState(e);
    }

    slotStart(e){
        this.setState({start: e.toJSON()});
    }

    slotEnd(e){
        this.setState({end: e.toJSON()});
    }

    addSlot(e){

        var slot = {start: this.state.start, end: this.state.end};


        var a = new Date(this.state.start), b = new Date(this.state.end);
        if(b-a > 6*60*60*1000){
            alert('Duration of slot should not be more than 6 hours');
            return;
        }
        if(b-a <= 0){
            alert('End Time of slot should not be less start time of the slot ');
            return;
        }

        var resource = {

            'summary': 'React Tutor',
            'location': 'Hackerrank Bangalore',
            'description': 'Calender of Tutor',
            'start': {
                'dateTime': slot.start,
                'timeZone': 'Asia/Calcutta'
            },
            'end': {
                'dateTime': slot.end,
                'timeZone': 'Asia/Calcutta'
            }
        };

        superagent.post('/insertEvent')
            .send({slot: slot, email: this.state.email})
            .end((err, response) => {
                if(err){
                    if(response && response.text) alert(response.text);
                    else alert('Error inserting event to calendar');
                }
                else{
                    alert('Event successfully inserted');
                }
            });
    }

    handleAuthClick(event) {

        var that = this;
        this.setState({done: false});
        console.log('called handleAuthClick with', event);

        gapi.auth.authorize(
            {client_id: CLIENT_ID, scope: SCOPES, immediate: false, access_type: 'offline', response_type: 'code'},
            (authResult) => {

                var auth_code = gapi.auth.getToken().code;
                console.log('auth code is ', auth_code);
                console.log('authResult is ', authResult);

                superagent.get('/authorize')
                    .query({code: auth_code})
                    .send({code: auth_code})
                    .end((err, res) => {
                        if(err){
                            console.log('err is ', err);
                            console.log('Error in authentication');
                            alert('Authentication Failed');
                            that.setState({done: true});
                        }
                        else if(res){
                            console.log('got response from server ', res);
                            that.setState({user: JSON.parse(res.text).name, done: true, email: JSON.parse(res.text).email,
                                authDisplay : 'none', tableDisplay: 'block'});
                        }
                    });
            });
        return false;
    }

    onSignOut() {
        console.log('clicked signout');
        this.setState({done: false});
        gapi.auth.signOut();
        this.setState({user: 'false', authDisplay: 'block', tableDisplay: 'none', done: true});
    }

    getEvents(){
        superagent.get('/getEvents')
            .query({email: this.state.email})
            .end((err, res) => {
                if(err){
                    console.log('err is ', err);
                    console.log('Error in getting calender information');
                    this.setState({done: true});
                }
                else if(res){
                    console.log('got response from server ', res);
                    this.setState({user: JSON.parse(res.text).name, done: true, email: JSON.parse(res.text).email,
                        authDisplay : 'none', tableDisplay: 'block'});
                }
            });
    }

    render() {

        if(!this.state.done) return <h1>Loading... </h1>;

        return(
            <div>
                <div id="authorize-div" style={{display: this.state.authDisplay}}>
                    <span><h2>Authorize access to Google Calendar API</h2></span>
                    <p>
                        <button id="authorize-button" onClick={this.handleAuthClick}>
                            Authorize
                        </button>
                    </p>
                </div>

                {this.state.user && this.state.user != 'false' && <div style={{display: this.state.tableDisplay}}>

                    <h2>Hi {this.state.user.split(" ")[0]}</h2>
                    <h3>Please Add Slots to book your Calender</h3>

                    <table>
                        <tbody>
                        <tr><th>Slot Start Time</th><th>Slot Finish Time</th></tr>
                        <tr><td><DateTime onChange={this.slotStart}/></td><td><DateTime onChange={this.slotEnd}/></td></tr>
                        <tr>
                            <td className="addSlot"><button  onClick={this.addSlot}>Add Slot</button></td>
                            <td><button style={{'marginLeft': '170px'}} onClick={this.onSignOut}>Sign Out</button></td>
                        </tr>
                        </tbody>
                    </table>
                    <Link to={"/calendar/"+this.state.email}><button className="btn blue">Get Calender</button></Link>
                    {this.props.children}
                </div>}
            </div>
        )
    }
};

var element = React.createElement(Hello, {});
React.render(element, document.querySelector('.container'));