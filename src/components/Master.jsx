import React, {Component} from 'react';
import {Link} from 'react-router';

export default class Calendar extends Component {

    render(){
        return(
            <div>
                <h1>Welcome to React Tutor</h1>
                <p><Link to="/login">Login</Link></p>
                {this.props.children}
            </div>
        );
    }
};