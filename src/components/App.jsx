import React from 'react';
import Login from './Login.jsx';
import Calendar from './Calendar.jsx';
import NotFound from './NotFound.jsx';
import Master from './Master.jsx';
import { Router, Route, Link, browserHistory, IndexRedirect } from 'react-router'
import { render } from 'react-dom'
import createBrowserHistory from 'history/lib/createBrowserHistory'

var routes = (
    <Router history={createBrowserHistory()}>
        <Route path="/" component={Master}>
            <Route path="login" component={Login}/>
            <Route path="calendar/:userId" component={Calendar}/>
            <Route path='*' component={NotFound}/>
        </Route>
    </Router>
);

render(routes, document.querySelector('.container'));