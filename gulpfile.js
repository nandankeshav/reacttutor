var gulp = require('gulp');
var gutil = require('gulp-util');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var babelify = require('babelify');
var watchify = require('watchify');
//var reactify = require('reactify');
var notifier = require('node-notifier');
var server = require('gulp-server-livereload');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var nodemon = require('gulp-nodemon');

//var notify = require("gulp-notify");

var notify = function(error) {
    var message = 'In: ';
    var title = 'Error: ';

    if(error.description) {
        title += error.description;
    } else if (error.message) {
        title += error.message;
    }

    if(error.filename) {
        var file = error.filename.split('/');
        message += file[file.length-1];
    }

    if(error.lineNumber) {
        message += '\nOn Line: ' + error.lineNumber;
    }

    message += error.message;
    notifier.notify({title: title, message: message});
    console.log(error.message);

};

var bundler = browserify({
    entries: ['./src/components/App.jsx'],
    extensions: ['.jsx'],
    transform: [babelify],
    debug: true,
    cache: {},
    packageCache: {},
    fullPaths: true
});

function bundle() {
    return bundler
        .bundle()
        .on('error', notify)
        .pipe(source('dist/main.js'))
        .pipe(gulp.dest('./'))
}
bundler = watchify(bundler);
bundler.on('update', bundle);

gulp.task('build', function() {
    bundle()
});

gulp.task('serve', function(done) {

    nodemon({
        script: 'server.js',
        ext: 'js jade',
        env: {'NODE_ENV': 'qa'},
        watch: ['server.js', './src/**', './*.js', './sass/**/*.scss'],
        nodeArgs: ['--debug','--harmony'],
        delay: 2
    })
        .on('restart', function () {
            console.log('restarted!')
    });

    //gulp.src('')
     //   .pipe(server({
     //       livereload: {
     //       enable: true,
     //       filter: function(filePath, cb) {
     //           if(/main.js/.test(filePath)) {
     //               cb(true)
     //           } else if(/style.css/.test(filePath)){
     //               cb(true)
     //           }
	//	    }
	//    },
     //   open: true
	//}));
});

gulp.task('sass', function () {
    gulp.src('./sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('default', ['build', 'serve', 'sass', 'watch']);

gulp.task('watch', function () {
    gulp.watch('./sass/**/*.scss', ['build', 'sass']);
    gulp.watch('./gulpfile.js', ['build', 'serve', 'sass']);
    gulp.watch('./src/**', ['build', 'serve', 'sass']);
    gulp.watch('./*.js', ['build', 'serve', 'sass']);
});
