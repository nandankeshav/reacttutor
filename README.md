# ReactTutor

### Prerequisites to run the app ###
**node version >= 4.2.4**

**npm version >=  2.14.12**

**MongoDB should be running with default configuration**
### To run the application please run the following commands ##

```
#!shell

git clone https://bitbucket.org/nandankeshav/reacttutor.git

cd ReactTutor

npm install

gulp

```
If gulp is not installed then please install it using the command 
```
#!shell

npm install -g gulp
```


### Tasks to be done ###
1. When the tutor signs up, you request access to the tutor's google calendar.
2. Next you ask for the time slots during the week in which the tutor is ready to teach. It could be like 9AM-5PM on Monday, 2PM - 6PM on Tuesday, etc.
3. You then give a link which the tutor can share with all his students to schedul
e sessions.
4. When a student views the link, he will see a calendar with the slots in which he can schedule a session.
5. To schedule a session, the student selects a 1 hour time slot and enters his name and email address.
6. After this, an event is created in the Tutor's google calendar for the selected time slot with the student's details.
7. Going forward, if someone goes to the tutor's URL which he shared with the candidates, he should see that slot as blocked.
#######################

## The app now supports all the requirements listed above##
### The tutor has flexibility to schedule the slots which may not be in quantum of 1 hour and hence the calendar page doesn't list only 1 hour slots. It lists all the events which are scheduled by tutor and they are not fragmented in smaller slots. This is open to change if it's an absolute requirement ###

## Improvements ##

* **Componentization** **(done)**
Implement separate components for Login and User. The former will handle user authentication while the latter will enable a Tutor to schedule slots. A component for calendar will facilitate student interaction with the calendar of tutor.

* **React Routers** **(done)**
Use React Routers to transition between separate pages so that multiple if else statements are avoided and the user experience is smooth.

* **Implement Flux Architecture** **(pending)**
All the user data that resides in the states of components should be moved to stores. Further the actions of a tutor and the students need to created and the state should be updated as store changes.

* **Improve the styling of the app** **(in progress)**
The current style of app is very rudimentary and definitely a lot can be done in this regard to improve the user experience.

* **Implement auto-refresh in the Calendar Page** **(pending)**
The calendar page of a user currently refreshes itself if the booking is made by the user but it doesn't refresh when the booking is changed by another user. Currently a student has to manually refresh the calendar page to get the latest status of slots. Going forward, this shall be automated so that the page automatically refreshes when a booking is changed elsewhere.

* **Refactor the code and add documentation** **(in progress)**
The codebase of app needs a lot of refactoring and proper documentation is not available currently. I shall keep on working on this.

* **Deployment** **(pending)**
The app has to be run locally and it isn't deployed publicly. I shall work on deploying it on Google App Engine/ Heroku/ Github

* **Testing** **(pending)**
I haven't implemented testing of the app. It needs to be done for both client side and server side.