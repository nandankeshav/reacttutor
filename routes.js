var express = require('express');
var router = express.Router(), app = router;
var oauth = require('oauth');
var superagent = require('superagent');
var assert = require('assert');
var clientId = '133551892961-p9p0a5gg752o8k3uj4g7k2ps8c8erkv5.apps.googleusercontent.com';
var clientSecret = 'OwbeIqjrgmTzUlhsDCStR_Af';
var bodyParser = require('body-parser');
var gcal = require('google-calendar');
var mongo = require('mongodb');
var mongoUri = process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || 'mongodb://localhost/default';
var database;

var defaultFunction = function(req, res, next){
    console.log('default called');
    res.render('index', {});
    next();
};

var calendarList = {

    "Wed Jan 06 2016":
        [
            {"start":"2016-01-06T18:30:00.000Z","end":"2016-01-06T18:30:00.000Z", "booked":false},
            {"start":"2016-01-06T20:30:00.000Z","end":"2016-01-06T21:30:00.000Z", "booked":false}
        ],
    "Thu Jan 07 2016":
        [
            {"start":"2016-01-07T18:30:00.000Z","end":"2016-01-07T19:30:00.000Z", "booked":false},
            {"start":"2016-01-07T20:30:00.000Z","end":"2016-01-07T21:30:00.000Z", "booked":false}
        ],
    "Fri Jan 08 2016":
        [
            {"start":"2016-01-08T19:30:00.000Z","end":"2016-01-08T20:30:00.000Z", "booked":true}
        ],
    "Sat Jan 09 2016":
        [
            {"start":"2016-01-09T09:30:00.000Z","end":"2016-01-09T12:30:00.000Z", "booked":false}
        ]
};

function connect(callback){

    var url = 'mongodb://localhost:27017/myproject';
    mongo.MongoClient.connect(url, function(err, db){
        if(err) {
            console.log('error connecting to database', err);
            return;
        }
        database = db;
    });
    callback();
}

router.use(bodyParser.json());
router.use('/auth', (request, response) => {
    console.log('request in GET authorize is ', request.body);
});

function getAccessToken(settings, callback){

    var accessToken;
    if((new Date()).getTime() < settings.google_access_token_expiration)
    {
        console.log('token not expired');
        callback(settings);
    }
    else{

        var oa = new oauth.OAuth2(clientId,
            clientSecret,
            "https://accounts.google.com/o",
            "/oauth2/auth",
            "/oauth2/token");

        oa.getOAuthAccessToken(settings.google_refresh_token, {grant_type:'refresh_token', client_id: clientId, client_secret: clientSecret}, function(err, access_token, refresh_token, res) {

            var expiresIn = parseInt(res.expires_in);
            var accessTokenExpiration = new Date().getTime() + (expiresIn * 1000);

            if (!refresh_token) settings.google_refresh_token = refresh_token;
            settings.google_access_token = access_token;
            settings.google_access_token_expiration = accessTokenExpiration;
            console.log('refreshed token');
        })
    }
}

router.get('/getMockEvents', (request, response) => {
    response.status(200).send(calendarList);
});

router.post('/bookEvent', (request, response) => {
    console.log('slot is ', request.body.slot);
    var slot = request.body.slot, day = (new Date(slot.start.slice(0, 19)+'+05:30')).toDateString();
    var email = request.query.email, studentName = request.query.studentName, studentEmail = request.query.studentEmail;
    var url = 'mongodb://localhost:27017/myproject';

    mongo.MongoClient.connect(url, function (err, db) {

        if (err) {
            console.log('error connecting to database', err);
            return;
        }
        if (db) {
            var collection = db.collection('tutor');
            //console.log('result from db is ', collection.findOne({google_user_id: googleUserId}) );
            collection.findOne({google_user_id: email}, function (findError, settings) {

                if (settings == null) {
                    response.status(404).send('User with email ' + email + ' not found');
                }
                else {
                    console.log('events is ', settings.events);
                    //response.status(200).send(settings.events);

                    var list = settings.events[day];
                    list.map((event) => {
                        if(event.start == slot.start && event.end == slot.end){
                            if(event.booked) response.status(400).send('Slot already booked');
                            else {

                                var resource = {

                                    'summary': 'React Tutor',
                                    'location': 'Hackerrank Bangalore',
                                    'description': 'Name of student is '+studentName+' and his email is '+studentEmail,
                                    'start': {
                                        'dateTime': slot.start,
                                        'timeZone': 'Asia/Calcutta'
                                    },
                                    'end': {
                                        'dateTime': slot.end,
                                        'timeZone': 'Asia/Calcutta'
                                    }
                                };



                                    var accessToken = settings.google_access_token;
                                    if((new Date()).getTime() < settings.google_access_token_expiration)
                                    {
                                        console.log('token not expired');
                                    }
                                    else{

                                        var oa = new oauth.OAuth2(clientId,
                                            clientSecret,
                                            "https://accounts.google.com/o",
                                            "/oauth2/auth",
                                            "/oauth2/token");

                                        oa.getOAuthAccessToken(settings.google_refresh_token, {grant_type:'refresh_token', client_id: clientId, client_secret: clientSecret}, function(err, access_token, refresh_token, res) {

                                            var expiresIn = parseInt(res.expires_in);
                                            var accessTokenExpiration = new Date().getTime() + (expiresIn * 1000);

                                            if (!refresh_token) settings.google_refresh_token = refresh_token;
                                            settings.google_access_token = accessToken = access_token;
                                            settings.google_access_token_expiration = accessTokenExpiration;
                                            console.log('refreshed token');
                                        })
                                    }


                                    console.log('accessToken is ', accessToken);
                                    var google_calendar = new gcal.GoogleCalendar(accessToken);
                                    console.log('****** ADDING GOOGLE EVENT *******');
                                    google_calendar.events.insert(email, resource, function (addEventError, addEventResponse) {
                                        console.log('GOOGLE RESPONSE:', addEventError, addEventResponse);

                                        if (!addEventError) {
                                            //response.status(200).send(addEventResponse);

                                            event.booked = true;
                                            collection.save(settings, (err, res) => {
                                                if (err) {
                                                    response.status(400).send('Error in booking slot');
                                                }
                                                else {
                                                    response.status(200).send('Slot successfully booked');
                                                }
                                            });

                                            return;
                                        }
                                        console.log('Error inserting event to Google Calendar', addEventError.errors[0].message);
                                        response.status(400).send(addEventError);
                                    });


                            }
                        }
                    });
                }
            });
        }
    });

});

router.get('/getEvents', (request, response) => {

    var email = request.query.email;
    console.log('email is ', email);
    var url = 'mongodb://localhost:27017/myproject';

    mongo.MongoClient.connect(url, function (err, db) {

        if (err) {
            console.log('error connecting to database', err);
            return;
        }
        if (db) {
            var collection = db.collection('tutor');
            //console.log('result from db is ', collection.findOne({google_user_id: googleUserId}) );
            collection.findOne({google_user_id: email}, function (findError, settings) {

                if (settings == null) {
                    response.status(404).send('User with email ' + email + ' not found');
                }
                else {
                    console.log('events is ', settings.events);
                    response.status(200).send(settings.events);
                }
            });
        }
    })
});

router.post('/insertEvent', (request, response) => {
    var slot = request.body.slot, email = request.body.email;
    console.log('slot is ', slot);
    console.log('email is ', email);
    var url = 'mongodb://localhost:27017/myproject';

    mongo.MongoClient.connect(url, function (err, db) {

        if (err) {
            console.log('error connecting to database', err);
            return;
        }
        if (db) {
            var collection = db.collection('tutor');
            //console.log('result from db is ', collection.findOne({google_user_id: googleUserId}) );
            collection.findOne({google_user_id: email}, function (findError, settings) {
                if (settings == null) {
                    response.status(404).send('User with email ' + email + ' not found');
                    return;
                }
                else {
                    var events = settings.events, day = (new Date(slot.start.slice(0, 19))).toDateString(),
                        total = (new Date(slot.end).valueOf()) - (new Date(slot.start).valueOf());
                    if(!events) settings.events = events = {};
                    var eventDate = events[day];
                    if(!eventDate) settings.events[day] = eventDate = [];

                    for(var i in eventDate){
                        var event = eventDate[i];
                        var a = (new Date(slot.start).valueOf()), b = (new Date(slot.end).valueOf()),
                            c = (new Date(event.start).valueOf()), d = (new Date(event.end).valueOf());
                        if(Math.max(a, c) < Math.min(b, d)){
                            response.status(400)
                                .send('Slot conflicts with already booked slots');
                            return;
                        }
                        total += d-c;
                    }
                    if(total > 6*60*60*1000){
                        response.status(400).send('Total slot time exceeeding 6 hours for the day');
                        return;
                    }
                    slot.booked = false;
                    settings.events[day].push(slot);
                    collection.save(settings);
                    response.status(200).send('Event successfully inserted');
                }
            });
        }
    })
});

router.get('/getGoogleEvents', (request, response) => {
    var email = request.query.email;

    console.log('email is ', email);


    var url = 'mongodb://localhost:27017/myproject';

    mongo.MongoClient.connect(url, function (err, db) {

        if (err) {
            console.log('error connecting to database', err);
            return;
        }
        if (db) {


            var collection = db.collection('tutor');

            //console.log('result from db is ', collection.findOne({google_user_id: googleUserId}) );


            collection.findOne({google_user_id: email}, function (findError, settings) {

                console.log('GOOGLE SETTINGS RESPONSE:', settings, findError);

                var accessTokenExpiration = new Date().getTime() + (3500 * 1000);

                if (settings == null) {
                    response.status(404).send('User with email ' + email + ' not found');
                }
                else {
                    var accessToken = settings.google_access_token;
                    console.log('accessToken from MongoDB is ', accessToken);
                    var google_calendar = new gcal.GoogleCalendar(accessToken);
                    console.log('****** GETTING GOOGLE EVENTS *******');
                    google_calendar.events.list(email, {'timeMin': new Date().toISOString()}, function(err, eventList){
                        if(err){
                            response.send(500, err);
                        }
                        else{
                            response.writeHead(200, {"Content-Type": "application/json"});
                            response.write(JSON.stringify(eventList, null, '\t'));
                            response.end();
                        }
                    });
                }
            });


        }
    })

});

router.post('/insertGoogleEvent', (request, response) => {
    var slot = request.body.slot, email = request.body.email;
    console.log('slot is ', slot);
    console.log('email is ', email);
    var resource = {

        'summary': 'React Tutor',
        'location': 'Hackerrank Bangalore',
        'description': 'Calender of Tutor',
        'start': {
            'dateTime': slot.start,
            'timeZone': 'Asia/Calcutta'
        },
        'end': {
            'dateTime': slot.end,
            'timeZone': 'Asia/Calcutta'
        }
    };

    var url = 'mongodb://localhost:27017/myproject';

    mongo.MongoClient.connect(url, function (err, db) {

        if (err) {
            console.log('error connecting to database', err);
            return;
        }
        if (db) {


            var collection = db.collection('tutor');

            //console.log('result from db is ', collection.findOne({google_user_id: googleUserId}) );


            collection.findOne({google_user_id: email}, function (findError, settings) {

                console.log('GOOGLE SETTINGS RESPONSE:', settings, findError);

                var accessTokenExpiration = new Date().getTime() + (3500 * 1000);

                if (settings == null) {
                    response.status(404).send('User with email ' + email + ' not found');
                }
                else {
                    var accessToken = settings.google_access_token;
                    console.log('accessToken from MongoDB is ', accessToken);
                    var google_calendar = new gcal.GoogleCalendar(accessToken);
                    console.log('****** ADDING GOOGLE EVENT *******');
                    google_calendar.events.insert(email, resource, function (addEventError, addEventResponse) {
                        console.log('GOOGLE RESPONSE:', addEventError, addEventResponse);

                        if (!addEventError) {
                            response.status(200).send(addEventResponse);
                            return;
                        }
                        console.log('Error inserting event to Google Calendar', addEventError.errors[0].message);
                        response.status(400).send(addEventError);
                    });
                }
            });
        }
    })
});

function getAccessToken(){

}

router.get('/authorize', (request, response) => {


    console.log('request body is ', request.body);
    //resonse.send(request);
    console.log('auth code in node is ', request.query.code);

    var oa = new oauth.OAuth2(clientId,
        clientSecret,
        "https://accounts.google.com/o",
        "/oauth2/auth",
        "/oauth2/token"), myResponse = response, profileInfo, googleUserId;

    oa.getOAuthAccessToken(request.query.code, {grant_type:'authorization_code', redirect_uri:'postmessage'}, function(err, access_token, refresh_token, res) {
        if (err) {
            console.log('error in access token ', err);
            //response.end('error: ' + JSON.stringify(err));
            console.log('response from google server is ', res);
        }
        else {
            console.log('access_token is ', access_token);
            console.log('refresh_token is ', refresh_token);

            superagent.get('https://www.googleapis.com/oauth2/v2/userinfo')
                .set('Authorization', 'Bearer ' + access_token)
                //.field('access_token', access_token)
                .end((err, res) => {
                    if (err) console.log('error in getting profile info', err.text);
                    if (res) {
                        profileInfo = res.body;
                        console.log('profile info is', profileInfo);
                        googleUserId = res.body.email;
                        response.status(200).send(JSON.stringify(profileInfo));
                        //connect();

                        var url = 'mongodb://localhost:27017/myproject';
                        mongo.MongoClient.connect(url, function (err, db) {
                            if (err) {
                                console.log('error connecting to database', err);
                                return;
                            }
                            if (db) {


                                var collection = db.collection('tutor');

                                //console.log('result from db is ', collection.findOne({google_user_id: googleUserId}) );


                                collection.findOne({google_user_id: googleUserId}, function (findError, settings) {

                                    console.log('GOOGLE SETTINGS RESPONSE:', settings, findError);

                                    var accessTokenExpiration = new Date().getTime() + (3500 * 1000);

                                    if (settings == null) {
                                        settings = {
                                            google_user_id: googleUserId,
                                            google_access_token: access_token,
                                            google_access_token_expiration: accessTokenExpiration,
                                            events: {}
                                        };
                                        if(typeof refresh_token != undefined){
                                            settings.google_refresh_token = refresh_token;
                                            console.log('refresh_token inserted is '+refresh_token);
                                        }
                                        collection.insert(settings, function (err, result) {
                                        if (err) console.log('Error inserting into Database', err.text);
                                        else {
                                            console.log('result after database operation is ', result);
                                        }
                                        });
                                    }
                                    else {
                                        settings.google_access_token = access_token;
                                        settings.google_access_token_expiration = accessTokenExpiration;

                                        collection.save(settings);
                                    }
                                });
                            }
                        });
                    };
                });
        }
    });

});

router.get('/access', function (req, res, next) {
    res.send('Howdy, You got the access!');
});

router.use([(req, res, next) => {
    console.log('calling from routes');
    console.log('request body is initially ', req.body);
    next();
}, defaultFunction
]);



module.exports = router;