
export const port = process.env.PORT || 8000;
export const host = process.env.WEBSITE_HOSTNAME || `localhost:${port}`;