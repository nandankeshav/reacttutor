//require("babel-core/register");
//import 'babel-core/polyfill';
var path = require('path');
var express = require('express');
var React = require('react');
var Router = require('./routes');
var oauth = require('oauth');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var clientId = '133551892961-p9p0a5gg752o8k3uj4g7k2ps8c8erkv5.apps.googleusercontent.com';
var clientSecret = 'OwbeIqjrgmTzUlhsDCStR_Af';
//import { port } from './config';

const app = global.server = express();

//
// Register Node.js middleware
// -----------------------------------------------------------------------------
app.use(express.static(path.join(__dirname, 'public')));

//
// Register API middleware
// -----------------------------------------------------------------------------
//server.use('/api/content', require('./api/content'));


//
// Launch the server
// -----------------------------------------------------------------------------
app.set('views', path.join(__dirname, 'src/views'));
app.set('view engine', 'jade');
app.use('/data', express.static('src/data'));
app.use('/images', express.static('src/images'));
app.use('/app/tutor/static', express.static('dist'));
app.use(Router);

//app.use(express.logger());
app.use(cookieParser());
//app.use(bodyParser.json());
//app.use(express.methodOverride());
app.use(express.static(__dirname + '/public'));

// START THE SERVER
var http = require('http');
var port = process.env.NODE_PORT || 3000;


http.createServer(app).listen(port, (err) => {
    if (err) {
        console.error("[ERROR]", err);
    }
    else {
        console.log('REACT TUTOR APP SERVER is running. port: %s, env: %s, service-log-level:%s', port, app.get('env'), process.env.NODE_SERVICE_LOG);
    }
});
